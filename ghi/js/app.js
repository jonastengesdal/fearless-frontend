function createCard(name, description, pictureUrl, starts, ends, locationName) {
    return `
    <div class="d-flex align-items-start flex-column mb-3">
    <div class="shadow p-3 mb-5 bg-white rounded">

        <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
            <p class="card-text">${description}</p>
            </div>
        </div>
        <div class="card-footer">
            <small class= "text-muted">${starts} - ${ends}</small>
        </div>
    </div>
    </div>
    `;
  }

function alert() {
    return `

    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Well done!</h4>
        <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
        <hr>
        <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
    </div>

    `
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // console.log("hello");
        let html = alert();

        // console.log(html, 'hello');
        const alerts = document.querySelector('.navbar-brand');
        alerts.innerHTML += html;


        // console.error("Response error")
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const startsDate = new Date(details.conference.starts);
            const starts = startsDate.getMonth() + "/" + startsDate.getDate() + "/" + startsDate.getFullYear();
            const endsDate = new Date(details.conference.ends);
            const ends = endsDate.getMonth() + "/" + endsDate.getDate() + "/" + endsDate.getFullYear();
            const locationName = details.conference.location.name;
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl, starts, ends, locationName);

            const column = document.querySelector('.row-cols-3');
            column.innerHTML += html;
            // console.log(html);
          }
        }

      }

    } catch (e) {
        console.error("You got an error");


      // Figure out what to do if an error is raised
    // var alertList = document.querySelectorAll('.alert')
    // var alerts =  [].slice.call(alertList).map(function (element) {
    // return new bootstrap.Alert(element)
    }
});
